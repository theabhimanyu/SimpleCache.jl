module SimpleCache
import Serialization
using InteractiveUtils
using CodeTracking
import MacroTools
import Pluto

export set_cache_path, @cached

Simple_Cache_Path = joinpath(".", "julia_simple_cache")

if ! @isdefined macro_cache
    macro_cache = Dict()
end

# We need a data type that no method has a specialisation for. So we declare
# CacheFakeStruct. Don't write a function that takes this as an argument
struct CacheFakeStruct
end

"""
If you call this function before running @cached, then the cache will be written into `path`.
Otherwise, it defaults to `./julia_simple_cache`.
"""
function set_cache_path(path)
    global Simple_Cache_Path
    Simple_Cache_Path = path
end

function argtypes_from_signature(sig)
    str = string(sig)
    ind = findfirst(",", str)
    if ind == nothing
        return []
    end
    str = str[ind[1]-1:end-1]

    depth = 0
    splitpoints = []
    for i in 1:length(str)
        c = str[i]
        if c == ',' && depth == 0
            push!(splitpoints, i)
        elseif c == '{'
            depth += 1
        elseif c == '}'
            depth -= 1
        end
    end
    push!(splitpoints, length(str)+1)

    types = []
    for i in 1:length(splitpoints)-1
        type = Main.eval(Meta.parse(str[splitpoints[i]+1:splitpoints[i+1]-1]))
        push!(types, type)
    end
    return tuple(types...)
end

function get_ast_of_meth(meth)
    argtypes = argtypes_from_signature(meth.sig)
    inner_ast = code_string(Main.eval(meth.name), argtypes)
    return Meta.parse(inner_ast)
end

ObjectID = typeof(objectid("hello computer"))
expr_hash(e::Expr) = objectid(e.head) + mapreduce(p -> objectid((p[1], expr_hash(p[2]))), +, enumerate(e.args); init=zero(ObjectID))
expr_hash(x) = objectid(x)

function hash_function_recursive(already_visited, ast)
    ast = MacroTools.striplines(ast)
    ret = expr_hash(ast)
    if ret in already_visited
        return 0
    end
    push!(already_visited, ret)

    refs = Pluto.ExpressionExplorer.try_compute_symbolreferences(ast)
    for (key,value) in refs.funcdefs
        for el in value.references
            global_variable = Main.eval(el)
            ret = hash((ret, global_variable))
        end
        for el in value.funccalls
            funcsym = :(Main)
            for i in 1:length(el)
                temp = el[i]
                funcsym = :($funcsym.$temp)
            end
            if contains(string(funcsym), "@")
                continue
            end
            func = nothing
            try
                func = Main.eval(funcsym)
            catch ex
                @assert typeof(ex) == UndefVarError
                continue
            end

            meths = methods(func)
            if endswith(split(string(meths), "\n")[1], "for type constructor:")
                continue
            end
            for meth in meths
                if meth.module != Main
                    continue
                end
                ret = hash((ret, hash_function_recursive(already_visited, get_ast_of_meth(meth))))
            end
        end
    end
    return ret
end

function cache_function(longname, funcast)
    func = Symbol(longname)
    @assert startswith(longname, "cached_")
    shortname = longname[length("cached_")+1:end]
    outer_func = Symbol(shortname)
    ret = :(function $(esc(outer_func))(args...)
        depshash = hash((args, hash_function_recursive([], $(QuoteNode(funcast)))))
        fp = joinpath($Simple_Cache_Path, string(depshash))
        if haskey(macro_cache, depshash)
            ret = macro_cache[depshash]
        elseif isfile(fp)
            cache = Base.open(fp, "r")
            ret = Serialization.deserialize(cache)
            Base.close(cache)
            macro_cache[depshash] = ret
        else
            ret = Main.$func(args...)
            cache = Base.open(fp, "w")
            Serialization.serialize(cache, ret)
            Base.close(cache)
            macro_cache[depshash] = ret
        end
        return ret
			end)
	ret
end

macro cached(func)
    if !isdir(Simple_Cache_Path)
        mkpath(Simple_Cache_Path)
        println("Created directory SimpleCache.Cache_Path == ", string(Simple_Cache_Path))
    end
    inner_func = "cached_" * string(func.args[1].args[1])
    temp = cache_function(inner_func, func)
    func.args[1].args[1] = Symbol(inner_func)
    return :(:block, $(esc(func)) , $temp)
end


end # module
